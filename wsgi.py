from flask import Flask, jsonify, render_template, request
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics
from dotenv import load_dotenv
import calendar
import socket
import os
import tempfile
import time
from pathlib import Path

load_dotenv('.env')

class Config(object):
  DEBUG = False
  APP_BG_COLOR = os.environ.get("APP_BG_COLOR")

# some cleanup from last start...
startup = Path(os.path.join(tempfile.gettempdir(), "startup"))
if startup.is_file():
    startup.unlink()

fail = Path(os.path.join(tempfile.gettempdir(), "fail"))
if fail.is_file():
    fail.unlink()

# create application
application = Flask(__name__)
application.config.from_object(Config)

# Prometheus Metrics
metrics = GunicornPrometheusMetrics(application)
metrics.info('k8s_demo','K8S Demo', version='1.0.0')

by_path_counter = metrics.counter(
    'by_path_counter',
    'Request count by request path',
    labels={'path': lambda: request.path}
)

@application.route("/")
@by_path_counter
def index():
    return render_template('index.html')

@application.route("/nodename")
@by_path_counter
def nodename():
    ret_nodename = socket.gethostname()
    return ret_nodename, 200

@application.route("/healthz")
@by_path_counter
def healthz():
    if Path(os.path.join(tempfile.gettempdir(), "fail")).is_file():
        return "Failure", 418
    else:
        p = Path(os.path.join(tempfile.gettempdir(), "startup"))
        if p.is_file():
            mtime = p.stat().st_mtime
            current_time = time.time()
            if (current_time - mtime) > 30:
                return "OK", 200
            else:
                return "Service Unavailable", 503
        else:
            return "OK", 200

@application.route("/healthz/startup")
@by_path_counter
def healthz_startup():
    """A startup probe indicates whether the application within a container is started.
    All other probes are disabled until the startup succeeds.
    """
    p = Path(os.path.join(tempfile.gettempdir(), "startup"))
    if not p.is_file():
        p.touch()
    return healthz()

@application.route("/healthz/liveness")
@by_path_counter
def healthz_liveness():
    """A liveness probe determines if a container is still running.
    Consequence: Pod is killed
    """
    return healthz()

@application.route("/healthz/readiness")
@by_path_counter
def healthz_readiness():
    """A readiness probe determines if a container is ready to accept service requests
    Consequence: Endpoint is removed from service
    """
    p = Path(os.path.join(tempfile.gettempdir(), "load"))
    if p.is_file():
        mtime = p.stat().st_mtime
        current_time = time.time()
        if (current_time - mtime) > 30:
            p.unlink()
        return "Gateway Timeout", 504
    else:
        return "OK", 200

@application.route("/unhealthz")
@by_path_counter
def unhealthz():
    return "I'm a teapot", 418

@application.route("/setfailure")
@by_path_counter
def setfailure():
    Path(os.path.join(tempfile.gettempdir(), "fail")).touch()
    return "OK", 200

@application.route("/setload")
@by_path_counter
def setload():
    Path(os.path.join(tempfile.gettempdir(), "load")).touch()
    return "OK", 200

if __name__ == "__main__":
    
    application.run()

