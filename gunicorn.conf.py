import os
from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics

wsgi_app = "wsgi:application"

workers = int(os.environ.get('GUNICORN_PROCESSES', '5'))
threads = int(os.environ.get('GUNICORN_THREADS', '1'))

bind = "0.0.0.0:8080"
forwarded_allow_ips = '*'
secure_scheme_headers = { 'X-Forwarded-Proto': 'https' }

errorlog = "-"
accesslog = "-"


#os.environ["PROMETHEUS_MULTIPROC_DIR"] = "/tmp/"

def when_ready(server):
    GunicornPrometheusMetrics.start_http_server_when_ready(9200)

def child_exit(server, worker):
    GunicornPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid)
