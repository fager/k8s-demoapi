FROM registry.access.redhat.com/ubi9/ubi:latest

RUN dnf install -y --disableplugin=subscription-manager python3 python3-pip && \
	dnf clean all && mkdir /opt/app-src

WORKDIR /opt/app-src/

ADD . /opt/app-src/

RUN pip3 install --no-cache-dir -r requirements.txt

ENV PROMETHEUS_MULTIPROC_DIR "/tmp/"

EXPOSE 8080
EXPOSE 9200

CMD ["/usr/local/bin/gunicorn"]

